import React, { useEffect, useState } from "react";

export const ManuFactList = () => {
  const [manufactList, setManufactList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      if (response.ok) {
        const data = await response.json();
        setManufactList(data.manufacturers);
      }
    };
    getData();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Name</th>
        </tr>
      </thead>
      <tbody>
        {manufactList.map((manufactList, idx) => (
          <tr key={idx}>
            <td>{manufactList.name}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
