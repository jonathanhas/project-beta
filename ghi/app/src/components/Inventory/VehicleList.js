import React, { useEffect, useState } from "react";

export const VehicleList = () => {
  const [vehicleList, setVehicleList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:8100/api/models/");
      if (response.ok) {
        const data = await response.json();
        setVehicleList(data.models);
      }
    };
    getData();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Manufacturer</th>
          <th scope="col">Picture</th>
        </tr>
      </thead>
      <tbody>
        {vehicleList.map((vehicleList, idx) => (
          <tr key={idx}>
            <td>{vehicleList.name}</td>
            <td>{vehicleList.manufacturer.name}</td>
            <td>
              <img
                src={vehicleList.picture_url}
                alt="https://as1.ftcdn.net/v2/jpg/02/48/42/64/1000_F_248426448_NVKLywWqArG2ADUxDq6QprtIzsF82dMF.jpg"
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
