import React, { useState, useEffect } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState("");
  const [filterTerm, setFilterTerm] = useState("");
  const [filterCategory, setFilterCategory] = useState("vin");
  const [filteredApps, setFilteredApps] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value);
  };

  const handleFilterCategoryChange = (e) => {
    setFilterCategory(e.target.value);
  };

  const getFilteredApps = () => {
    setFilteredApps(
      appointments.filter((appointment) =>
        appointment[filterCategory]
          .toLowerCase()
          .includes(filterTerm.toLowerCase())
      )
    );
  };

  return (
    <>
      <div>
        <div className="row" style={{ width: "100%" }}>
          <div style={{ paddingLeft: 11 }} className="col-5">
            <h1>Service History</h1>
          </div>
        </div>
      </div>
      <div className="input-group mb-3 mt-3">
        <input
          onChange={handleFilterChange}
          type="text"
          className="form-control"
          placeholder="VIN"
          aria-describedby="basic-addon2"
        ></input>
        <div className="input-group-append">
          <button
            onClick={getFilteredApps}
            className="btn btn-outline-secondary"
            type="button"
          >
            Search VIN
          </button>
        </div>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Owner</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {filteredApps.map((appointment) => {
            return (
              <>
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.owner}</td>
                  <td>{appointment.date}</td>
                  <td>{appointment.technician.name}</td>
                  <td>{appointment.reason}</td>
                </tr>
              </>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ServiceHistory;
