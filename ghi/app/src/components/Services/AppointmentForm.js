import React, { useEffect, useState } from "react";

function AppointmentForm() {
  const [appointments, setAppointment] = useState([]);
  const [formData, setFormData] = useState({
    vin: "",
    owner: "",
    date: "",
    technician: "",
    reason: "",
  });

  const getData = async () => {
    const url = "http://localhost:8080/services/appointments/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointment(data.appointment);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const appointmentsUrl = "http://localhost:8080/services/appointments/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointmentsUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: "",
        owner: "",
        date: "",
        technician: "",
        reason: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="Vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">Vin</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.owner}
                placeholder="Owner"
                required
                type="text"
                name="owner"
                id="owner"
                className="form-control"
              />
              <label htmlFor="owner">Owner</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.date}
                placeholder="Date"
                required
                type="date"
                name="date"
                id="date"
                className="form-control"
              />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.technician}
                placeholder="Technician"
                required
                type="text"
                name="technician"
                id="technician"
                className="form-control"
              />
              <label htmlFor="technician">Technician</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.reason}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create Appointment</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
