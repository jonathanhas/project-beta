import React, { useEffect, useState } from "react";
function TechnicianForm() {
  const [msg, setMsg] = useState(null);
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    employee_number: "",
  });
  const getData = async () => {
    const url = "http://localhost:8080/services/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: formData.name,
      employee_number: formData.employee_number,
    };
    const locationUrl = "http://localhost:8080/services/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
        picture_url: "",
        manufacturer: "",
      });
    }
  };
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Technician</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  value={formData.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  value={formData.picture_url}
                  placeholder="Employee Number"
                  required
                  type="text"
                  name="employee_number"
                  id="employee_number"
                  className="form-control"
                />
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
export default TechnicianForm;