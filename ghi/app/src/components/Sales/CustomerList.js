import React, { useEffect, useState } from "react";

export const CustomerList = () => {
  const [customerList, setCustomerList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:8090/sales/customer/");
      if (response.ok) {
        const data = await response.json();
        setCustomerList(data.customer);
      }
    };
    getData();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Address</th>
          <th scope="col">Phone Number</th>
        </tr>
      </thead>
      <tbody>
        {customerList.map((customerList, idx) => (
          <tr key={idx}>
            <th scope="row">{idx + 1}</th>
            <td>{customerList.name}</td>
            <td>{customerList.address}</td>
            <td>{customerList.phone_number}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
