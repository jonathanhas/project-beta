import React, { useState } from "react";

function CustomerForm() {
  const [vin, setVIN] = useState("");
  const [salesperson, setSalesPerson] = useState("");
  const [employee_number, setEmployeeNumber] = useState("");
  const [customer, setCustomer] = useState("");
  const [sales_price, setSalesPrice] = useState("");
  const [msg, setMsg] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8090/sales/createrecord/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({ vin, salesperson, employee_number, customer, sales_price}),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setVIN("");
      setSalesPerson("");
      setEmployeeNumber("");
      setCustomer("");
      setSalesPrice("");
      setMsg("Record created!");
    }
  };

  return (
    <>
      <form className="container p-5" onSubmit={handleSubmit}>
        {msg !== null && (
          <div className="alert alert-success" role="alert">
            {msg}
          </div>
        )}
        <div className="mb-3">
          <label className="form-label">VIN</label>
          <input
            type="text"
            id="vin"
            className="form-control"
            value={vin}
            onChange={(event) => setVIN(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Sales Person</label>
          <input
            type="text"
            id="sales_person"
            className="form-control"
            value={salesperson}
            onChange={(event) => setSalesPerson(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Employee Number</label>
          <input
            type="text"
            id="employee_number"
            className="form-control"
            value={employee_number}
            onChange={(event) => setEmployeeNumber(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Customer</label>
          <input
            type="text"
            id="customer"
            className="form-control"
            value={customer}
            onChange={(event) => setCustomer(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Sales Price</label>
          <input
            type="text"
            id="sales_price"
            className="form-control"
            value={sales_price}
            onChange={(event) => setSalesPrice(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create
        </button>
      </form>
    </>
  );
}

export default CustomerForm;
