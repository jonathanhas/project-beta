from django.urls import path

from .views import api_create_salesPerson, api_create_customer, api_create_record, api_list_salesrecord, api_list_saleshistory

urlpatterns = [    
    path("salesperson/", api_create_salesPerson, name="create_salesperson"),
    path("customer/", api_create_customer, name="create_customer"),
    path("createrecord/", api_create_record, name="create_record"),
    path("salesrecord/", api_list_salesrecord, name="list_sales"),
    path("saleshistory/", api_list_saleshistory, name="sales_history"),
]
