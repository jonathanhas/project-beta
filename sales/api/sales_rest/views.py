from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import SalesPersonEncoder, CustomerEncoder,  SalesRecordEncoder, SalesListEncoder
from .models import SalesPerson, Customer, SalesRecord



@require_http_methods(["GET", "POST"])
def api_create_salesPerson(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse({"sales_person": sales_person}, encoder=SalesPersonEncoder)
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)



@require_http_methods(["GET", "POST"])
def api_create_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse({"customer": customer}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, encoder=CustomerEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_create_record(request):
    if request.method == "GET":
        createrecord = SalesRecord.objects.all()
        return JsonResponse({"create_record": createrecord}, encoder=SalesRecordEncoder)
    else:
        content = json.loads(request.body)
        createrecord = SalesRecord.objects.create(**content)
        return JsonResponse(createrecord, encoder=SalesListEncoder, safe=False)



@require_http_methods(["GET"])
def api_list_salesrecord(request):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.all()
            return JsonResponse({"sales_record": sales_record}, encoder=SalesListEncoder, safe=False)
        except:
            return JsonResponse({"message": "Does not exist"}, status=404)


@require_http_methods(["GET"])
def api_list_saleshistory(request):
    if request.method == "GET":
        try:
            sales_history = SalesRecord.objects.all()
            return JsonResponse({"sales_history": sales_history}, encoder=SalesRecordEncoder, safe=False)
        except:
            return JsonResponse({"message": "Does not exist"}, status=404)